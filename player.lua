Player = Object:extend()

function Player:new()
  --appareance
    self.img = love.graphics.newImage("images/player.png")
    self.x = 150
    self.y = 300
  --speed
    self.xSpeed = 0
    self.ySpeed = 0
    self.acc =130
    self.maxSpeed=600
    self.friction = 20
    self.gravity = 30
  --jump

   self.isJumping = false -- are we in the process of jumping?
   self.isGrounded = false -- are we on the ground?
   self.hasReachedMax = false -- is this as high as we can go?
   self.jumpAcc = 215 -- how fast do we accelerate towards the top
   self.jumpMaxSpeed = 600 -- our speed limit while jumping
   self.filter = function(item, other)
    
  end
end

function Player:update(dt,world)
  recover=0
  local goalX = self.x + self.xSpeed
  local goalY = self.y + self.ySpeed
  
  if self.y<=0 then
    player.hasReachedMax =  true
  else
    player.hasReachedMax = false
  end

  if self.y+self.img:getHeight()>=719 then
    player.isGrounded = true
  else
    player.isGrounded = false
  end
  
  for j, bullet in ipairs(bullets) do
    if bullet.speed>0 then
      if CheckCollision( player.x+10, player.y, player.img:getWidth()-5, player.img:getHeight(),
      bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight())  then
        bullet.speed=bullet.speed*-1
        bullet.acc=0
        bullet.start = -1
        bullet.x=bullet.x+45
      end
    end
  end

  for i, booster in ipairs(boosters) do
           
    if CheckCollision(player.x, player.y, player.img:getWidth(), player.img:getHeight(),
            booster.x, booster.y, 61, 63) then 
      score= score + 10
      if booster.type==1 then
        recover = recover + booster.power
      else
        player.acc = player.acc + (booster.power/3)
      end

      table.remove(boosters, i)
    end
        
  end
  self.x, self.y, collisions = world:move(player, goalX, goalY,self.filter)
 --[[  for i, coll in ipairs(collisions) do
    if coll.touch.y > goalY then

    elseif coll.normal.y < 0 then

    end

    
  end ]]


  --friction
  self.xSpeed=self.xSpeed *(1- math.min(dt*self.friction, 1))
  self.ySpeed=self.ySpeed *(1- math.min(dt*self.friction, 1))

  --gravity
  if self.isGrounded ==false then
    self.ySpeed = self.ySpeed+self.gravity*dt

    if(love.keyboard.isDown("left","a") and self.xSpeed > -self.maxSpeed and self.x>=150) then
      self.xSpeed = self.xSpeed -self.acc*dt
    elseif love.keyboard.isDown("right","d") and self.xSpeed<self.maxSpeed and self.x<=350 then
      self.xSpeed = self.xSpeed + self.acc*dt
    elseif love.keyboard.isDown("down","s") and self.ySpeed<self.maxSpeed then
      self.ySpeed = self.ySpeed + self.acc*dt
    end
  end

  --jump
  if love.keyboard.isDown("up","w") then
    if -self.ySpeed<self.jumpMaxSpeed and not self.hasReachedMax then
      self.ySpeed = self.ySpeed -self.jumpAcc *dt
    elseif math.abs(self.ySpeed)>self.jumpMaxSpeed then
      self.hasReachedMax = true
    end
    self.isGrounded=false
  
  end
  return recover
end

function Player:draw( )
    love.graphics.draw(self.img, self.x, self.y)
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end