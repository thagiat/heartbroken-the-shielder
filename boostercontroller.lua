BoosterController = Object:extend()

function BoosterController:new()
    require "booster"
end

function BoosterController:update(dt,world)
    
    createBoosterTimer = createBoosterTimer - (1 * dt)
    power=20

      if createBoosterTimer < 0 then
        createBoosterTimer = createBoosterTimerMax
        if score >= 15 and score <50 then
            createBoosterTimerMax = 18
            power = 30
        elseif score>=50 then
            createBoosterTimerMax = 15
            power = 40
        end

        randomNumber = math.random(10, 610)
        randomNumber2 = math.random(1,2)
        booster=Booster(-50,randomNumber,power,randomNumber2)
        
        table.insert(boosters, booster)
        
      end
    
      for i, booster in ipairs(boosters) do
        
        booster.x = booster.x + (booster.speed * dt)
      
        if booster.x > 1024 then 
            table.remove(boosters, i)
        end
            
        end

end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
    return x1 < x2+w2 and
           x2 < x1+w1 and
           y1 < y2+h2 and
           y2 < y1+h1
  end