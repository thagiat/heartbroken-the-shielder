bump = require 'libs.bump.bump'

world = nil 


love.graphics.setDefaultFilter('nearest','nearest')

bullets = {}
enemies = {}
boosters={}
score = 0

function love.load()
  Object = require "libs/classic"
  require "enemies"
  require "player"
  require "boostercontroller"

  -- Setup bump
  isAlive = false
  life = 100
  justStarted=true
  createEnemyTimerMax=6
  createEnemyTimer=0
  createSuperEnemyTimerMax=45
  createSuperEnemyTimer=40
  createBoosterTimer = 20
  createBoosterTimerMax = 20

  math.randomseed( os.time() )
  world = bump.newWorld(32)  
  
  player=Player()
  enemies_controller=Enemies()
  booster_controller=BoosterController()

  world:add(player, player.x, player.y, player.img:getWidth(), player.img:getHeight())

  font = love.graphics.newFont("Graduate.ttf", 48)
  fontbig=love.graphics.newFont("Graduate.ttf", 78)
  love.graphics.setFont(font)

  background = love.graphics.newImage("images/fundo.png")
  ending = love.graphics.newImage("images/ending.png")
  lifebar1 = love.graphics.newImage("images/lifebar1.png")
  lifebar2 = love.graphics.newImage("images/lifebar2.png")
  lifebar3 = love.graphics.newImage("images/lifebar3.png")
  lifebar4 = love.graphics.newImage("images/lifebar4.png")
  lifebar5 = love.graphics.newImage("images/lifebar5.png")
  lifebar6 = love.graphics.newImage("images/lifebar6.png")
  capa = love.graphics.newImage("images/capa.png")  
  heartbooster= love.graphics.newImage("images/heartbooster.png")
  speedbooster = love.graphics.newImage("images/speedbooster.png")
end


function love.update(dt)

  if isAlive==true then
    booster_controller:update(dt,world)
    liferecovered = player:update(dt,world)
    
    life = life + liferecovered
    --[[ if life > 100 then
      life=100
    end ]]

    damagetaken=enemies_controller:update(dt,world)

    life = life - damagetaken
    if life <= 0 then
      isAlive=false
    end

  end
  --RESTART
  if not isAlive and love.keyboard.isDown('r') then
    love.graphics.setFont(font)


    bullets = {}
    enemies = {}
    boosters = {}

    if justStarted==true then
      justStarted=false
    end

    player.x = 150
    player.y = 300
  
    
    math.randomseed( os.time() )
    score = 0
    isAlive = true
    life=100
    createEnemyTimerMax=8
    createEnemyTimer=0
    createBoosterTimer = 20
    createBoosterTimerMax = 20

  end
end


function love.draw()
  if justStarted==true then
    love.graphics.draw(capa, 0, 0)
  elseif isAlive==true then

    love.graphics.draw(background, 0, 0)
    
    love.graphics.print(score, 900, 20, 0, 1, 1)

    player:draw()

    for i, enemy in ipairs(enemies) do
      love.graphics.draw(enemy.img, enemy.x, enemy.y)
    end

    for i, bullet in ipairs(bullets) do
      if bullet.speed>=0 then
        love.graphics.draw(bullet.img, bullet.x, bullet.y)
      else
        love.graphics.draw(bullet.img, bullet.x, bullet.y,0,-1,1)
      end
    end

    for i, booster in ipairs(boosters) do
      if booster.type==1 then
        love.graphics.draw(heartbooster, booster.x, booster.y)
      else
        love.graphics.draw(speedbooster, booster.x, booster.y)
      end
      
    end

    if life >= 100 then
      love.graphics.draw(lifebar1, 10, 20)
      elseif life>=80 then
        love.graphics.draw(lifebar2, 10, 20)
      elseif life>=60 then
        love.graphics.draw(lifebar3, 10, 20)
      elseif life>=40 then
        love.graphics.draw(lifebar4, 10, 20)
      elseif life>=20 then
        love.graphics.draw(lifebar5, 10, 20)
      elseif life>=0 then
        love.graphics.draw(lifebar6, 10, 20)
    end

    if life == 100 then
      love.graphics.print(life, 45, 75, 0, 1, 1)
    else
      love.graphics.print(life, 60, 75, 0, 1, 1)
    end

  else
    love.graphics.setFont(fontbig)
    love.graphics.draw(ending, 0, 0)
    love.graphics.print(score, 700, 535, 0, 1, 1)
  end

  
end