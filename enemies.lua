Enemies = Object:extend()

function Enemies:new()
    require "enemy"
    require "bullet"
end

function Enemies:update(dt,world)
    damage=0
    createEnemyTimer = createEnemyTimer - (1 * dt)
    createSuperEnemyTimer = createSuperEnemyTimer - (1 * dt)

    if  createSuperEnemyTimer < 0 then
        createSuperEnemyTimer = createSuperEnemyTimerMax
        randomNumber = math.random(10, 610)
        
        enemy=Enemy(1050,randomNumber)
        enemy.img=love.graphics.newImage("images/enemysuper.png")
        enemy.super=true
        enemy.life=120
        table.insert(enemies, enemy)
        world:add(enemy, enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight())
    end


      if createEnemyTimer < 0 then
        createEnemyTimer = createEnemyTimerMax
        if score > 3 and score <10 then
            createEnemyTimerMax = 4
        elseif score >= 15 and score <50 then
            createEnemyTimerMax = 3
        elseif score>=50 then
            createEnemyTimerMax = 2
            createSuperEnemyTimerMax=35
        elseif score>=150 then
            createEnemyTimerMax = 1.5
        elseif score>=250 then
            createEnemyTimerMax = 1
            createSuperEnemyTimerMax=25
        end

        randomNumber = math.random(10, 610)
        
        enemy=Enemy(1024,randomNumber)
        table.insert(enemies, enemy)
        world:add(enemy, enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight())
      end
    
      for i, enemy in ipairs(enemies) do
        
        enemy.x = enemy.x - (enemy.speed * dt)
      
        if enemy.x < 600 then 
          enemy.x=600
        end

        if score > 3 and score <10 then
            enemy.cooldown= 2
            enemy.speed = 60
            enemy.point = 3 
        elseif score >= 15 and score <50 then
            enemy.cooldown= 1
            enemy.speed = 70
            enemy.point = 5 
        elseif score>=50 then
            enemy.cooldown= 0.75
            enemy.speed = 80
            enemy.point = 7 
        elseif score>=150 then
            enemy.cooldown= 0.5
            enemy.speed = 90
            enemy.point = 10 
        end

        enemy.canshoot = enemy.canshoot - (1 * dt)
        
        for j, bullet in ipairs(bullets) do
            
            if CheckCollision(bullet.x, bullet.y, bullet.img:getWidth()-30, bullet.img:getHeight(),
                enemy.x+25, enemy.y, enemy.img:getWidth(), enemy.img:getHeight()-10) 
                and enemy.isalive==true then
                    
                    enemy.life = enemy.life - bullet.damage
                    table.remove(bullets, j)
            end
            
        end

        if enemy.life<=0 then
            score= score + enemy.point
            enemy.isalive=false
            table.remove(enemies, i)
        end

        if enemy.canshoot < 0 and enemy.isalive==true then
            
            if enemy.super==true then
                enemy.canshoot = 0.5
                for i = 2,1,-1 do 
                    randomNumber = math.random(enemy.y-50,enemy.y+enemy.img:getHeight()+50)
                    bullet=Bullet(enemy.x-75,randomNumber)
                    table.insert(bullets, bullet)
                    world:add(bullet,bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight())
                end
            else
                enemy.canshoot = enemy.cooldown
                randomNumber = math.random(enemy.y-10,enemy.y+enemy.img:getHeight()+10)
                bullet=Bullet(enemy.x-75,randomNumber)
                table.insert(bullets, bullet)
                world:add(bullet,bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight())
            end
        end

      end

      
        for i, bullet in ipairs(bullets) do

            if score > 3 and score <10 then
                bullet.damage = 15
                bullet.acc = 10
            elseif score >= 15 and score<50 then
                bullet.damage = 20
                bullet.acc = 15
            elseif score>=50 then
                bullet.damage = 25
                bullet.acc = 20
            elseif score>=200 then
                bullet.damage = 30
                bullet.acc = 20
            end

            if bullet.speed>=0 then
                bullet.speed= bullet.speed+bullet.acc
            else 
                bullet.speed= bullet.speed-bullet.acc
            end 

            if bullet.speed > bullet.maxspeed then
                bullet.speed=bullet.maxspeed
            end

            
            if bullet.start>0 then
                bullet.start = bullet.start - (1 * dt)
            end 

            bullet.x = bullet.x - (bullet.speed * dt)

            --se o player deixar passar
            if bullet.x < 150 then 
                damage=damage+ bullet.damage
                table.remove(bullets, i)
            end
            
        end
    

    return damage
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
    return x1 < x2+w2 and
           x2 < x1+w1 and
           y1 < y2+h2 and
           y2 < y1+h1
  end